//! Structure that contains the state of a snake body.

use crate::xil;
use crate::print64;
use crate::println64;

use crate::part::Part as Part;
use crate::part::Direction as Direction;
use crate::point::Point as Point;

// =============================================================================
// LOCAL CONSTANTS
// =============================================================================

/// Maximum amount of body parts.
const BODY_PART_COUNT_MAX: usize = 64;

/// Default head position at start.
const POSITION_HEAD_START: Point = Point::new(0, 0);

/// Default head direction at start.
const DIRECTION_HEAD_START: Direction = Direction::South;

//==============================================================================
// TYPE DEFINITIONS
//==============================================================================

/// Array of body parts.
type BodyType = [Part; BODY_PART_COUNT_MAX];

//==============================================================================
// BODY DEFINITION
//==============================================================================

pub struct Body {
    body: BodyType,
    length: usize,
}

impl Body {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> Body {
        Body {
            body: [Part::new(POSITION_HEAD_START, DIRECTION_HEAD_START);
                    BODY_PART_COUNT_MAX],
            length: 1,
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn length(&self) -> usize {
        self.length
    }

    pub fn part_exists(&self, index: usize) -> bool {
        index <= self.length - 1
    }
    
    pub fn head(&self) -> &Part {
        &self.body[0]
    }

    pub fn part_at(&self, index: usize) -> &Part {
        &self.body[index]
    }

    pub fn tail(&self) -> &Part {
        &self.body[self.length - 1]
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================

    pub fn part_at_mut(&mut self, index: usize) -> &mut Part {
        &mut self.body[index]
    }

    pub fn head_mut(&mut self) -> &mut Part {
        self.part_at_mut(0)
    }
        
    pub fn add_part(&mut self, position: Point, direction: Direction) -> bool {
        if self.length <= BODY_PART_COUNT_MAX {
            self.add_new_tail(position, direction);
            self.set_length(self.length + 1);
            println64!("added part {} to {} with direction {}",
                self.length - 1,
                position,
                direction);
            return true;
        }
        else {
            println64!("could not add part {}, body is full", self.length + 1);
            return false;
        }
    }

    // =========================
    // PRIVATE MUTABLE INTERFACE
    // =========================

    fn add_new_tail(&mut self, position: Point, direction: Direction) {
        self.body[self.length] = Part::new(position, direction);
    }

    fn set_length(&mut self, length: usize) {
        self.length = length
    }
}
