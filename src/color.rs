//! Discrete position in the RGB color space.
//! Considered to be constant.

//==============================================================================
// COLOR DEFINITION
//==============================================================================

#[derive(Clone, Copy)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new(red: u8, green: u8, blue: u8) -> Color {
        Color {
            red: red,
            green: green,
            blue: blue}
    }
    
    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn red(&self) -> u8 {
        self.red
    }
    
    pub fn green(&self) -> u8 {
        self.green
    }
    
    pub fn blue(&self) -> u8 {
        self.blue
    }
}
