//! Structure that contains the state of the game and handles game state
//! transitions. Must be initialized with `initialize` before use.

use core::fmt;

use crate::xil;
use crate::print64;
use crate::println64;

use crate::snake::Snake as Snake;
use crate::point::Point as Point;
use crate::part::Direction as Direction;
use crate::tilemap::Tilemap as Tilemap;
use crate::tile::Tile as Tile;
use crate::pointlist::PointList as PointList;

//==============================================================================
// TYPE DEFINITIONS
//==============================================================================

/// Snake score.
type Score = u8;

//==============================================================================
// LOCAL CONSTANTS
//==============================================================================

const WALL_DETECTION_RANGE: usize = 2;
const FOOD_DETECTION_RANGE: usize = 2;

//==============================================================================
// SNAKEUPDATERESULT ENUMERATION
//==============================================================================

/// What happened to the snake during the update?
#[derive(Clone, Copy)]
enum SnakeUpdateResult {
    SnakeSurvived,
    SnakeDied,
}

//==============================================================================
// FOODUPDATERESULT ENUMERATION
//==============================================================================

/// What happened to the food during the update?
enum FoodUpdateResult {
    FoodRelocated,
    SnakeFilledSpace,
    NothingHappened,
}

//==============================================================================
// GAMESTATUS ENUMERATION
//==============================================================================

/// Current state of the game.
#[derive(Clone, Copy)]
pub enum GameStatus {
    NotStarted,
    Running,
    Paused,
    Finished,
}

impl fmt::Display for GameStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            GameStatus::NotStarted => write!(f, "not started"),
            GameStatus::Running => write!(f, "running"),
            GameStatus::Paused => write!(f, "paused"),
            GameStatus::Finished => write!(f, "finished"),
        }
    }
}

//==============================================================================
// TRANSITIONRESULT ENUMERATION
//==============================================================================

/// What happened during the game state transition to the next state?
pub enum TransitionResult {
    Okay,
    GameNotStarted,
    GamePaused,
    GameFinished,
    SnakeDied,
    SnakeWon,
}

//==============================================================================
// HELPER FUNCTIONS
//==============================================================================

fn manhattan(point1: Point, point2: Point) -> usize {
    let x1: isize = point1.x() as isize;
    let y1: isize = point1.y() as isize;
    let x2: isize = point2.x() as isize;
    let y2: isize = point2.y() as isize;

    ((x1 - x2).abs() + (y1 - y2).abs()) as usize
}

// ====================
// GAMESTATE DEFINITION
// ====================

pub struct GameState {
    /// Collection of discrete positions at player space.
    tilemap: Tilemap,

    /// Structure that contains the state of player controlled snake.
    snake: Snake,

    /// Position of the food.
    food: Point,

    /// Current state of the game.
    game_status: GameStatus,

    /// Current score of the player.
    score: Score,
}

impl GameState {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> GameState {
        GameState {
            tilemap: Tilemap::new(),
            snake: Snake::new(),
            food: Point::new(5, 5),
            game_status: GameStatus::NotStarted,
            score: 0
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn game_status(&self) -> GameStatus {
        self.game_status
    }
        
    pub fn tilemap_tile_at(&self, position: Point) -> Tile {
        self.tilemap.tile_at(position).clone()
    }
    
    pub fn score(&self) -> Score {
        self.score
    }

    /// Check if snake head is near a wall.
    pub fn is_head_near_wall(&self) -> bool {
        let sx: usize = self.snake.head().position().x();
        let sy: usize = self.snake.head().position().y();
        let w: usize = self.tilemap.width();
        let h: usize = self.tilemap.height();

        if (0 .. WALL_DETECTION_RANGE).contains(&sx) { return true; }
        if (w - WALL_DETECTION_RANGE .. w).contains(&sx) { return true; }
        if (0 .. WALL_DETECTION_RANGE).contains(&sy) { return true; }
        if (h - WALL_DETECTION_RANGE .. h).contains(&sy) { return true; }
        false
    }

    /// Check if snake head is near a food.
    pub fn is_head_near_food(&self) -> bool {        
        let distance: usize = manhattan(self.snake.head().position(),
                                        self.food);
        distance <= FOOD_DETECTION_RANGE
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================

    pub fn initialize(&mut self) {
        self.tilemap.set_tile(self.snake.head().position(), Tile::new_snake());
        self.tilemap.set_tile(self.food, Tile::new_food());
    }

    pub fn start_game(&mut self) {
        match self.game_status {
            GameStatus::NotStarted => {
                self.set_game_status(GameStatus::Paused);
            },
            GameStatus::Running => {
                println64!("could not start game, game already started");
            },
            GameStatus::Paused => {
                println64!("could not start game, game already started");
            },
            GameStatus::Finished => {
                println64!("could not start game, game already finished");
            },
        }
    }

    pub fn pause_game(&mut self) {
        match self.game_status {
            GameStatus::NotStarted => {
                println64!("could not pause game, game not started");
            },
            GameStatus::Running => {
                self.set_game_status(GameStatus::Paused);
                println64!("game paused");
            },
            GameStatus::Paused => {
                println64!("could not pause game, game already paused");
            },
            GameStatus::Finished => {
                println64!("could not pause game, game already finished");
            },
        }
    }

    pub fn resume_game(&mut self) {
        match self.game_status {
            GameStatus::NotStarted => {
                println64!("could not resume game, game not started");
            },
            GameStatus::Running => {
                println64!("could not resume game, game already running");
            },
            GameStatus::Paused => {
                self.game_status = GameStatus::Running;
                println64!("game resumed");
            },
            GameStatus::Finished => {
                println64!("could not resume game, game already finished");
            },
        }
    }

    pub fn set_snake_direction(&mut self, direction: Direction) {
        self.snake.set_head_direction(direction);
        println64!("snake head direction changed from {} to {}",
            self.snake.head().direction_old(),
            self.snake.head().direction());
    }

    pub fn increase_score(&mut self) {
        let old_score: Score = self.score;
        self.score = self.score + 1;
        println64!("score changed from {} to {}",
            old_score,
            self.score);
    }

    /// Execute state transition.
    pub fn tick(&mut self) -> TransitionResult {
        match self.game_status {
            GameStatus::NotStarted => {
                return TransitionResult::GameNotStarted;
            },
            GameStatus::Running => {
                return self.transition_state();
            },
            GameStatus::Paused => {
                return TransitionResult::GamePaused;
            },
            GameStatus::Finished => {
                return TransitionResult::GameFinished;
            },
        }  
    }

    // =================
    // PRIVATE INTERFACE
    // =================
    
    fn head_and_food_collides(&self) -> bool {
        let head: Point = self.snake.head().position();
        let equal_x: bool = head.x() == self.food.x();
        let equal_y: bool = head.y() == self.food.y();
        equal_x && equal_y
    }
    
    // =========================
    // PRIVATE MUTABLE INTERFACE
    // =========================

    /// Execute state transition.
    fn transition_state(&mut self) -> TransitionResult {
        
        // -----------------------------------
        // advance to and interpret next state
        // -----------------------------------
        
        match self.update_snake() {
            SnakeUpdateResult::SnakeSurvived => {
                // business as usual
            },
            SnakeUpdateResult::SnakeDied => {
                self.set_game_status(GameStatus::Finished);
                return TransitionResult::SnakeDied;
            },
        }

        match self.update_food() {
            FoodUpdateResult::NothingHappened => {
                // business as usual
            },
            FoodUpdateResult::FoodRelocated => {
                // business as usual
            },
            FoodUpdateResult::SnakeFilledSpace => {
                self.set_game_status(GameStatus::Finished);
                return TransitionResult::SnakeWon;
            },
        }

        if !self.draw_food() {
            println64!("could not draw food");
            self.set_game_status(GameStatus::Finished);
            return TransitionResult::SnakeDied;
        }

        if !self.draw_snake() {
            println64!("could not draw snake");
            self.set_game_status(GameStatus::Finished);
            return TransitionResult::SnakeDied;
        }

        TransitionResult::Okay
    }

    fn set_game_status(&mut self, status: GameStatus) {
        let old_status: GameStatus = self.game_status;
        self.game_status = status;
        println64!("game status changed from {} to {}",
            old_status,
            self.game_status);
    }

    fn move_snake(&mut self) -> bool {
        if !self.snake.advance() {
            println64!("snake could not move from {} to {}",
                self.snake.head().position_old(),
                self.snake.head().direction());
            return false;
        }

        println64!("snake moved from {} to {}",
            self.snake.head().position_old(),
            self.snake.head().position());
        true
    }
    
    fn move_food(&mut self, position: Point) -> bool {
        let old_position: Point = self.food;
        
        if !self.tilemap.tile_exists(position) {
            println64!("food could not be moved from {} to {}",
                old_position,
                position);
            return false;
        }

        self.tilemap.set_tile(position, Tile::new_food());
        self.food = position;
        println64!("food moved from {} to {}",
            old_position,
            self.food);
        true
    }

    fn relocate_food(&mut self) -> bool {
        let free_tiles: PointList = self.tilemap.free_points();
        if free_tiles.is_empty() {
            println64!("no free tiles for food");
            return false;
        }
        if !free_tiles.is_index_valid(0) {
            println64!("no free tiles for food, invalid index");
            return false;
        }

        // maximizing manhattan distance between head and new food location
        let mut best_index: usize = 0;
        let mut best_distance: usize = 0;
        let head: Point = self.snake.head().position();
        for index in 0..free_tiles.length() {
            if free_tiles.is_index_valid(index) {
                let position: Point = free_tiles.point_at(index);
                let distance: usize = manhattan(position, head);
                if best_distance < distance {
                    best_index = index;
                    best_distance = distance;
                }
            }
        }
        println64!("new food distance to head is {}", best_distance);
        let new_position = free_tiles.point_at(best_index);
        return self.move_food(new_position);
    }
    
    fn update_food(&mut self) -> FoodUpdateResult {
        if !self.head_and_food_collides() {
            return FoodUpdateResult::NothingHappened;
        }

        println64!("head {} and food {} collides",
            self.snake.head().position(),
            self.food);
        self.increase_score();

        if !self.snake.add_part() {
            // snake has filled player space
            println64!("could not add part");
            return FoodUpdateResult::SnakeFilledSpace;
        }

        println64!("added part, snake length is {} and score {}",
            self.snake.body().length(),
            self.score());

        if !self.relocate_food() {
            // snake has filled player space
            println64!("could not relocate food");
            return FoodUpdateResult::SnakeFilledSpace;
        }

        println64!("food relocated");
        FoodUpdateResult::FoodRelocated
    }

    fn update_snake(&mut self) -> SnakeUpdateResult {
        if !self.move_snake() {
            println64!("snake hit the wall, snake died");
            return SnakeUpdateResult::SnakeDied;
        }

        if self.snake.eats_itself() {
            println64!("snake ate itself, snake died");
            return SnakeUpdateResult::SnakeDied;
        }

        SnakeUpdateResult::SnakeSurvived
    }

    fn draw_food(&mut self) -> bool {
        if !self.tilemap.tile_exists(self.food) {
            println64!("tile {} does not exist, could not draw food",
                self.food);
            return false;
        }

        self.tilemap.set_tile(self.food, Tile::new_food());
        true
    }

    fn draw_snake(&mut self) -> bool {
        for index in 0..self.snake.body().length() {
            if !self.snake.body().part_exists(index) {
                println64!("part {} does not exist, could not draw snake",
                    index);
                return false;
            }

            let part = self.snake.body().part_at(index);

            if !self.tilemap.tile_exists(part.position()) {
                println64!("tile {} does not exist, could not draw snake",
                    part.position());
                self.set_game_status(GameStatus::Finished);
                return false;
            }

            self.tilemap.clear_tile(part.position_old());
            self.tilemap.set_tile(part.position(), Tile::new_snake());
        }
        true
    }
}
