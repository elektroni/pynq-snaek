//! Structure that contains the state of a single snake part.

use core::fmt;

use crate::point::Point as Point;

//==============================================================================
// DIRECTION ENUMERATION
//==============================================================================

#[derive(Clone, Copy)]
pub enum Direction {
    North,
    South,
    East,
    West,
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Direction::North => write!(f, "north"),
            Direction::South => write!(f, "south"),
            Direction::East => write!(f, "east"),
            Direction::West => write!(f, "west"),
        }
    }
}

//==============================================================================
// PART DEFINITION
//==============================================================================

#[derive(Clone, Copy)]
pub struct Part {
    position: Point,
    position_old: Point,
    direction: Direction,
    direction_old: Direction,
}

impl Part {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new(position: Point, direction: Direction) -> Part {
        Part {
            position: position,
            position_old: position,
            direction: direction,
            direction_old: direction,
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn position(&self) -> Point {
        self.position
    }

    pub fn x(&self) -> usize {
        self.position.x()
    }

    pub fn y(&self) -> usize {
        self.position.y()
    }

    pub fn position_old(&self) -> Point {
        self.position_old
    }
    
    pub fn direction(&self) -> Direction {
        self.direction
    }
    
    pub fn direction_old(&self) -> Direction {
        self.direction_old
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================

    fn set_position(&mut self, position: Point) {
        self.position = position;
    }

    pub fn set_x(&mut self, x: usize) {
        let old_y: usize = self.position().y();
        let new_position: Point = Point::new(x, old_y);
        self.set_position(new_position);
    }

    pub fn set_y(&mut self, y: usize) {
        let old_x: usize = self.position().x();
        let new_position: Point = Point::new(old_x, y);
        self.set_position(new_position);
    }

    pub fn set_direction(&mut self, direction: Direction) {
        self.direction = direction;
    }

    pub fn set_position_old(&mut self, position_old: Point) {
        self.position_old = position_old;
    }

    pub fn set_direction_old(&mut self, direction_old: Direction) {
        self.direction_old = direction_old;
    }
}
