//! Bit manipulation functions.

use crate::bitindex::BitIndex as BitIndex;

/// Set given bit in byte to high.
pub fn set_bit(byte: u8, bit: BitIndex) -> u8 {
	// example: let byte = 0b01101001 and bit = 2

	// mask = 0b00000100
	let mask: u8 = 1 << bit.index();

	// result = 0b01101101
	return byte | mask;
}

/// Set given bit in a byte to low.
pub fn clear_bit(byte: u8, bit: BitIndex) -> u8 {
	// example: let byte = 0b01101001 and bit = 3

	// mask = 0b00001000
	let mut mask: u8 = 1 << bit.index();

	// mask = 0b11110111
	mask = !mask;

	// result = 0b01100001
	return byte & mask;
}

/// Set given bit in a byte to given value.
pub fn modify_bit(byte: u8, bit: BitIndex, state: bool) -> u8 {
    if state {set_bit(byte, bit)}
    else {clear_bit(byte, bit)}
}

/// Read a bit from given index at a byte.
pub fn read_bit(byte: u8, bit: BitIndex) -> bool {
	// move requested bit to right corner
	// example: let byte = 0b01101001 and bit = 2
	//          then result = 0b00011010
	let result: u8 = byte >> bit.index();

	// perform bitwise and to detect if requested bit is high
	// example: let result = 0b00001011
	//          then result = 0b00000001
    if result & 0b00000001 == 1 {true} else {false}
}

// I'm bitman
