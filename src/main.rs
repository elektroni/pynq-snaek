//! A classic snake game with Rust ran on PYNQ-Z1.
//! This project is derived from alien_shooter_rs crate.

// Roni Hämäläinen
// 284595
// 25.2.2021
// Course: Bare Metal Rust 2020

//==============================================================================
// COMPILER FLAGS
//==============================================================================

#![no_std]
#![feature(asm, lang_items, start)]

//==============================================================================
// MODULE DECLARATIONS
//==============================================================================

mod print;
mod interrupt;

mod tile;
mod body;
mod part;
mod color;
mod snake;
mod point;
mod bitman;
mod tilemap;
mod bitindex;
mod pointlist;
mod gamestate;
mod hardwarecontroller;

//==============================================================================
// USAGE DECLARATIONS
//==============================================================================

use xil_sys as xil;
use core::{ffi::c_void, panic::PanicInfo};

use crate::hardwarecontroller::Controller as Controller;
use crate::hardwarecontroller::ADDRESS_LED as ADDRESS_LED;
use crate::hardwarecontroller::ADDRESS_RGB as ADDRESS_RGB;
use crate::gamestate::GameStatus as GameStatus;
use crate::gamestate::GameState as GameState;
use crate::gamestate::TransitionResult as TransitionResult;
use crate::tilemap::TILEMAP_HEIGHT as TILEMAP_HEIGHT;
use crate::tilemap::TILEMAP_WIDTH as TILEMAP_WIDTH;
use crate::tile::Tile as Tile;
use crate::point::Point as Point;
use crate::color::Color as Color;
use crate::part::Direction as Direction;

//==============================================================================
// LOCAL CONSTANTS
//==============================================================================

/// Controls how often game state is updated.
const GS_UPDATE_INTERVAL: u8 = 8;

//==============================================================================
// DRAWSTATE ENUMERATION
//==============================================================================

/// What operation is performed when led matrix is updated.
#[derive(Clone, Copy)]
enum DrawState {
    /// Draw empty tilemap.
    DrawNothing,

    /// Draw snake parts and food.
    DrawTilemap,

    /// Draw victory effect.
    DrawWinEffect,

    /// Draw loss effect.
    DrawLoseEffect,
}

//==============================================================================
// GLOBAL MUTABLE STATES
//==============================================================================

/// Game state transitions when full.
static mut GS_UPDATE_COUNTER: u8 = 0;

/// Game state object.
static mut GS: GameState = GameState::new();

/// Hardware controller object.
static mut HC: Controller = Controller::new();

/// Selected operation for led matrix update.
static mut DRAW_STATE: DrawState = DrawState::DrawNothing;

//==============================================================================
// GLOBAL STATE FUNCTIONS
//==============================================================================

unsafe fn draw_win_effect() {
    for y in 0..TILEMAP_HEIGHT {
        for x in 0..TILEMAP_WIDTH {
            let position: Point = Point::new(x, y);
            HC.set_pixel(position, Color::new(0, 255, 0));
        }
    }
}

unsafe fn draw_lose_effect() {
    for y in 0..TILEMAP_HEIGHT {
        for x in 0..TILEMAP_WIDTH {
            let position: Point = Point::new(x, y);
            HC.set_pixel(position, Color::new(255, 0, 0));
        }
    }
}

unsafe fn draw_gamestate() {
    for y in 0..TILEMAP_HEIGHT {
        for x in 0..TILEMAP_WIDTH {
            let position: Point = Point::new(x, y);
            let tile: Tile = GS.tilemap_tile_at(position);
            HC.set_pixel(position, tile.color());
        }
    }
}

/// Execute game state transition and figure out what to draw next.
unsafe fn advance_gamestate() {
    DRAW_STATE = match GS.tick() {
        TransitionResult::GameNotStarted => DrawState::DrawTilemap,
        TransitionResult::Okay => DrawState::DrawTilemap,
        TransitionResult::GamePaused => DrawState::DrawTilemap,
        TransitionResult::SnakeDied => DrawState::DrawLoseEffect,
        TransitionResult::SnakeWon => DrawState::DrawWinEffect,
        TransitionResult::GameFinished => DRAW_STATE,
    }
}

unsafe fn update_led_matrix() {
    let draw: bool = match DRAW_STATE {
        DrawState::DrawNothing => {
            false
        },
        DrawState::DrawTilemap => {
            draw_gamestate();
            true
        },
        DrawState::DrawWinEffect => {
            draw_win_effect();
            true
        },
        DrawState::DrawLoseEffect => {
            draw_lose_effect();
            true
        },
    };
    
    if draw {
        for channel in 0..HC.size() {
            HC.update_channel(channel);
        }
    }
}

unsafe fn update_game_state() {
    // display tick progress via leds
    match GS_UPDATE_COUNTER {
        0..=1 => {
            core::ptr::write_volatile(ADDRESS_LED as *mut u8, 0b0001);
        },
        2..=3 => {
            core::ptr::write_volatile(ADDRESS_LED as *mut u8, 0b0011);
        },
        4..=5 => {
            core::ptr::write_volatile(ADDRESS_LED as *mut u8, 0b0111);
        },
        6..=7 => {
            core::ptr::write_volatile(ADDRESS_LED as *mut u8, 0b1111);
        },
        _ => {
            core::ptr::write_volatile(ADDRESS_LED as *mut u8, 0b0000);
        },
    }

    // display score via RGB leds
    if GS.is_head_near_wall() {
        if GS.is_head_near_food() {
            core::ptr::write_volatile(ADDRESS_RGB as *mut u8, 0b001001);
        }
        else {
            core::ptr::write_volatile(ADDRESS_RGB as *mut u8, 0b100100);
        }
    }
    else {
        if GS.is_head_near_food() {
            core::ptr::write_volatile(ADDRESS_RGB as *mut u8, 0b010010);
        }
        else {
            core::ptr::write_volatile(ADDRESS_RGB as *mut u8, 0b000000);
        }
    }

    if GS_UPDATE_COUNTER < GS_UPDATE_INTERVAL {
        GS_UPDATE_COUNTER += 1;
    }
    else {
        advance_gamestate();
        GS_UPDATE_COUNTER = 0;
    }
}

//==============================================================================
// TIMER INTERRUPT HANDLERS
//==============================================================================

/// Fast timer interrupt handler.
/// Used to update single led matrix channel per call.
pub unsafe extern "C" fn tick_handler(callback_ref: *mut c_void) {
    // disable exceptions during led matrix update
    xil::Xil_ExceptionDisable();
    
    update_led_matrix();

    // clear timer interrupt status
    // Cast `void*` received from the C API to the "Triple Timer Counter" (TTC)
    // instance pointer.
    // The C API needs to use void pointers to pass data around, because the C
    // specification does not describe abstract data types (ADT).
    let ttc = callback_ref as *mut xil::XTtcPs;
    let status_event = xil::XTtcPs_GetInterruptStatus(ttc);
    xil::XTtcPs_ClearInterruptStatus(ttc, status_event);

    // enable exceptions
    xil::Xil_ExceptionEnable();
}

/// Slow timer interrupt handler.
/// Used to time game state transitions.
pub unsafe extern "C" fn tick_handler_1(callback_ref: *mut c_void) {
    update_game_state();

    // clear timer interrupt status
    let ttc = callback_ref as *mut xil::XTtcPs;
    let status_event = xil::XTtcPs_GetInterruptStatus(ttc);
    xil::XTtcPs_ClearInterruptStatus(ttc, status_event);
}

//==============================================================================
// INPUT INTERRUPT HANDLERS
//==============================================================================

unsafe fn button0_pressed() {
    println64!("button0 pressed");

    GS.set_snake_direction(Direction::East);
}

unsafe fn button1_pressed() {
    println64!("button1 pressed");

    GS.set_snake_direction(Direction::North);
}

unsafe fn button2_pressed() {
    println64!("button2 pressed");

    GS.set_snake_direction(Direction::South);
}

unsafe fn button3_pressed() {
    println64!("button3 pressed");

    GS.set_snake_direction(Direction::West);
}

unsafe fn switch0_manipulated() {
    println64!("switch0 manipulated");
    
    // toggle snake moving ON or OFF
    match GS.game_status() {
        GameStatus::NotStarted => {
            println64!("could not pause/resume game, game not started");
        },
        GameStatus::Running => {
            GS.pause_game();
        },
        GameStatus::Paused => {
            GS.resume_game();
        },
        GameStatus::Finished => {
            println64!("could not pause/resume game, game finished");
        },
    }
}

unsafe fn switch1_manipulated() {
    println64!("switch1 manipulated");

    let reset: bool = match GS.game_status() {
        GameStatus::NotStarted => {
            println64!("could not restart game, game not started");
            false
        },
        GameStatus::Running => {
            true
        },
        GameStatus::Paused => {
            true
        },
        GameStatus::Finished => {
            true
        },
    };

    if reset {
        println64!("restarting game");
        GS = GameState::new();
        GS.initialize();
        DRAW_STATE = DrawState::DrawTilemap;
        HC.set_pixel(Point::new(5,5), Color::new(255,0,0));
        GS.start_game();
    }
}

/// Interrupt handler for switch and buttons.
/// Connected buttons are at bank 2.
///
/// # Arguments
///
/// * `status` - bit vector which identifies interrupt source
pub unsafe extern "C" fn button_handler(callback_ref: *mut c_void,
                                        _bank: u32,
                                        status: u32) {
    let _gpio = callback_ref as *mut xil::XGpioPs;

    match status {
        0b1 => button0_pressed(),
        0b10 => button1_pressed(),
        0b100 => button2_pressed(),
        0b1000 => button3_pressed(),
        0b10000 => switch0_manipulated(),
        0b100000 => switch1_manipulated(),
        _ => {},
    }
}

//==============================================================================
// MAIN FUNCTION
//==============================================================================

#[start]
fn main(_argc: isize, _argv: *const *const u8) -> isize {
    interrupt::init();
    
    unsafe {HC.initialize();}
    println64!("hardware controller initialized");
    
    unsafe {GS.initialize();}
    println64!("game state initialized");
    
    // enable led matrix updates
    unsafe {DRAW_STATE = DrawState::DrawTilemap;}
    
    unsafe {xil::Xil_ExceptionEnable();}
    
    // draw food
    unsafe {HC.set_pixel(Point::new(5,5), Color::new(255,0,0));}

    unsafe {GS.start_game();}
    
    loop {}
}

//==============================================================================
// PANIC HANDLER
//==============================================================================

/// A custom panic handler for Cortex-A9.
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // logs "panicked at '$reason', src/main.rs:27:4" to host stdout
    println64!("{}", info);
    loop {}
}
