//! Discrete position at player space.

use crate::color::Color as Color;

//==============================================================================
// GLOBAL CONSTANTS
//==============================================================================

pub const COLOR_EMPTY: Color = Color::new(0, 0, 0);
pub const COLOR_SNAKE: Color = Color::new(0, 255, 0);
pub const COLOR_FOOD: Color = Color::new(255, 0, 0);

//==============================================================================
// TILETYPE ENUMERATION
//==============================================================================

#[derive(Clone, Copy)]
pub enum TileType {
    Empty,
    Snake,
    Food,
}

//==============================================================================
// TILE DEFINITION
//==============================================================================

#[derive(Clone, Copy)]
pub struct Tile {
    ttype: TileType,
    color: Color,
}

impl Tile {

    // ============
    // CONSTRUCTORS
    // ============

    pub const fn new(ttype: TileType, color: Color) -> Tile {
        Tile {ttype: ttype, color: color}
    }
    
    pub const fn new_empty() -> Tile {
        Tile::new(TileType::Empty, COLOR_EMPTY)
    }

    pub const fn new_snake() -> Tile {
        Tile::new(TileType::Snake, COLOR_SNAKE)
    }

    pub const fn new_food() -> Tile {
        Tile::new(TileType::Food, COLOR_FOOD)
    }

    // ================
    // PUBLIC INTERFACE
    // ================

    pub fn ttype(&self) -> TileType {
        self.ttype
    }

    pub fn color(&self) -> Color {
        self.color
    }
}
