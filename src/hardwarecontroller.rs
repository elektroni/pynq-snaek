//! Handles writing to led matrix framebuffer.
//!
//! Assumptions:
//! - Target device uses DM163 current sink for the led matrix channels.
//! - Led matrix is a 8x8 pixel matrix.
//! - Pixel color space is RGB with 8 bit values.
//! - Pixel color correction space is 6 bits.

use crate::point::Point as Point;
use crate::color::Color as Color;
use crate::bitman::{modify_bit, read_bit};
use crate::bitindex::BitIndex as BitIndex;

//==============================================================================
// TYPE DEFINITIONS
//==============================================================================

/// Framebuffer is a 2D matrix of pixel color values.
type FrameBufferType =
    [[[u8; COLOR_VALUE_COUNT]; SCREEN_SIZE]; SCREEN_SIZE];

/// Write target selector (BIT_SB) states.
type WriteTarget = bool;

//==============================================================================
// LOCAL CONSTANTS
//==============================================================================

/// Amount of pixels in the side of the screen.
const SCREEN_SIZE: usize = 8;

/// Amount of color space dimensions.
const COLOR_VALUE_COUNT: usize = 3;

/// Amount of color correction bits.
const COLOR_CORRECTION_BIT_COUNT: usize = 6;

/// Amount of color value bits.
const COLOR_VALUE_BIT_COUNT : usize = 8;

// --------------------------
// memory mapped IO addresses
// --------------------------

/// Memory address of single color leds control bits.
pub const ADDRESS_LED: *mut u8 = 0x41200000 as *mut u8;

pub const ADDRESS_RGB: *mut u8 = 0x41240000 as *mut u8;
//const ADDRESS_INPUT: *mut u8 = 0xE000A068 as *mut u8;

/// Memory address of led matrix control bits.
const ADDRESS_CONTROL: *mut u8 = 0x41220008 as *mut u8;

/// Memory address of led matrix channel control bits.
const ADDRESS_CHANNEL: *mut u8 = 0x41220000 as *mut u8;

/// Possible open channel states.
const CHANNELS: [u8; SCREEN_SIZE] = [
    0b00000001,
    0b00000010,
    0b00000100,
    0b00001000,
    0b00010000,
    0b00100000,
    0b01000000,
    0b10000000,
];

/// Zeroed framebuffer.
const FRAMEBUFFER_DEFAULT:
    FrameBufferType = [[[0; COLOR_VALUE_COUNT]; SCREEN_SIZE]; SCREEN_SIZE];

//==============================================================================
// CONTROL BIT ENUMERATION
//==============================================================================

/// Control register bit indices.
#[repr(u8)]
enum ControlBit {
    /// Serial data in.
    SDA = 4,
    
    /// Serial clock in. Used to write data in.
    SCK = 3,
    
    /// Write target (correction or color).
    SB = 2,
    
    /// Latch. Normal low.
    LAT = 1,
    
    /// Reset. Normal high.
    RST = 0,
}

//==============================================================================
// WRITE TARGET ENUMERATION
//==============================================================================

/// SDA bits are written to led matrix 6-bit color correction memory.
const WRITE_TARGET_CORRECTION: WriteTarget = false;

/// SDA bits are written to led matrix 8-bit color value memory.
const WRITE_TARGET_COLOR: WriteTarget = true;

//==============================================================================
// MEMORY MAPPED IO FUNCTIONS
//==============================================================================

/// Set control bit in the control register.
/// Control register controls the led matrix.
fn set_control(control: ControlBit, state: bool) {
    let index: u8 = control as u8;
    let mut bit_index: BitIndex = BitIndex::new();
    if bit_index.set_index(index) {
        unsafe {
            let new_control: u8 = modify_bit(*ADDRESS_CONTROL,
                                             bit_index,
                                             state);
            core::ptr::write_volatile(ADDRESS_CONTROL, new_control);
        }
    }
}

/// Set write target where SDA bit is written to.
fn set_write_target(target: WriteTarget) {
    set_control(ControlBit::SB, target);
}

/// Close all led matrix channels.
fn close_channels() {
    unsafe {core::ptr::write_volatile(ADDRESS_CHANNEL, 0);}
}

/// Open single led matrix channel at given index.
fn open_channel(index: usize) {
    if SCREEN_SIZE <= index {
        close_channels();
        return;
    }
    else {
        unsafe {
            core::ptr::write_volatile(ADDRESS_CHANNEL, CHANNELS[index]);
        }
    }
}

//==============================================================================
// CONTROLLER DEFINITION
//==============================================================================

/// Controls the framebuffer and writing to the led matrix.
///
/// Notice! Must be initialized before use.
pub struct Controller {
    /// Led matrix pixel color value matrix.
    framebuffer: FrameBufferType,
}

impl Controller {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> Controller {
        Controller{
            framebuffer: FRAMEBUFFER_DEFAULT
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================

    pub fn size(&self) -> usize {
        SCREEN_SIZE
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================
    
    /// Reset shield and save 6-bit color correction values.
    pub fn initialize(&mut self) {
        set_control(ControlBit::RST, true);
        self.advance_clock();
        set_write_target(WRITE_TARGET_CORRECTION);
        for _channel in 0..SCREEN_SIZE {
            for _color in 0..COLOR_VALUE_COUNT {
                for _correction_bit in 0..COLOR_CORRECTION_BIT_COUNT {
                    self.write_data_bit(true);
                }
            }
            self.latch();
        }
        set_write_target(WRITE_TARGET_COLOR);
    }
    
    /// Set framebuffer pixel at given position to given RGB value.
    pub fn set_pixel(&mut self, position: Point, color: Color) {
        let x: usize = position.x();
        let y: usize = position.y();
        self.framebuffer[x][y][0] = color.red();
        self.framebuffer[x][y][1] = color.green();
        self.framebuffer[x][y][2] = color.blue();
    }
    
    /// Update given channel led matrix color values.
    pub fn update_channel(&mut self, x: usize) {

        // this kills the ghost pixel
        self.clear_latch();

        // plant new values for each pixel
        for y in 0..SCREEN_SIZE {

            // for each color byte, start from last byte
            for color in (0..COLOR_VALUE_COUNT).rev() {

                let byte: u8 = self.framebuffer[x][y][color];
                
                // for each byte bit, start from last bit
                for index in (0..COLOR_VALUE_BIT_COUNT).rev() {
                    
                    // build and solve bit index
                    let mut bit_index: BitIndex = BitIndex::new();
                    bit_index.set_index(index as u8);
                    let state = read_bit(byte, bit_index);

                    // write bit state
                    self.write_data_bit(state);
                }
            }
        }
        
        // save new values
        self.latch();
        
        // display new values
        open_channel(x);
    }
    
    // =================
    // PRIVATE INTERFACE
    // =================

    /// Flip SCK ON and OFF.
    fn advance_clock(&self) {
        set_control(ControlBit::SCK, true);
        set_control(ControlBit::SCK, false);
    }
    
    /// Write a bit to color data shift register.
    fn write_data_bit(&self, state: bool) {
        set_control(ControlBit::SDA, state);
        self.advance_clock();
    }
    
    /// Save shift register data to latch register.
    fn latch(&self) {
        set_control(ControlBit::LAT, true);
        set_control(ControlBit::LAT, false);
    }

    /// Clear latch register pixel color values.
    fn clear_latch(&self) {
        for _y in 0..SCREEN_SIZE {
            for _color in 0..COLOR_VALUE_COUNT {
                for _bit in 0..COLOR_VALUE_BIT_COUNT {
                    self.write_data_bit(false);
                }
            }
        }
        self.latch();
        close_channels();
    }
}
