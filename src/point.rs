//! Discrete position in a 2D plane.
//! Considered to be constant.

use core::fmt;

//==============================================================================
// POINT DEFINITION
//==============================================================================

#[derive(Clone, Copy)]
pub struct Point {
    x: usize,
    y: usize,
}

impl Point {

    // ============
    // CONSTRUCTORS
    // ============

    pub const fn new(x: usize, y: usize) -> Point {
        Point{x: x, y: y}
    }

    pub const fn new_empty() -> Point {
        Point::new(0, 0)
    }

    // ================
    // PUBLIC INTERFACE
    // ================

    pub fn x(&self) -> usize {
        self.x
    }

    pub fn y(&self) -> usize {
        self.y
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({},{})", self.x, self.y)
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x() && self.y == other.y()
    }
}
