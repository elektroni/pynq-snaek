//! Object that represents an index to a 8-bit byte.

//==============================================================================
// LOCAL CONSTANTS
//==============================================================================

const BIT_COUNT: u8 = 8;
const BIT_INDEX_MAX: u8 = BIT_COUNT - 1;

//==============================================================================
// BITINDEX DEFINITION
//==============================================================================

pub struct BitIndex {
    index: u8,
}

impl BitIndex {
    
    // ===========
    // CONSTRUCTOR
    // ===========
    
    pub const fn new() -> BitIndex {
        BitIndex{index: 0}
    }
    
    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn index(&self) -> u8 {
        self.index
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================
    
    pub fn set_index(&mut self, index: u8) -> bool {
        if BIT_INDEX_MAX < index {return false;}
        self.index = index;
        true
    }
}
