//! Array of points for each tilemap tile.

use crate::tilemap::TILEMAP_WIDTH as TILEMAP_WIDTH;
use crate::tilemap::TILEMAP_HEIGHT as TILEMAP_HEIGHT;
use crate::point::Point as Point;

//==============================================================================
// LOCAL CONSTANTS
//==============================================================================

const LENGTH_MAX: usize = TILEMAP_WIDTH * TILEMAP_HEIGHT;

//==============================================================================
// POINTLIST DEFINITION
//==============================================================================

pub struct PointList {
    points: [Point; LENGTH_MAX],
    length: usize,
}

impl PointList {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> PointList {
        PointList {
            points: [Point::new_empty(); LENGTH_MAX],
            length: 0
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn length(&self) -> usize {
        self.length
    }

    pub fn is_empty(&self) -> bool {
        self.length() == 0
    }
    
    pub fn is_full(&self) -> bool {
        self.length() == LENGTH_MAX
    }
    
    pub fn is_index_valid(&self, index: usize) -> bool {
        index <= self.length() - 1
    }
    
    /// Get point at given index.
    ///
    /// Warning! Given index must be valid.
    pub fn point_at(&self, index: usize) -> Point {
        self.points[index]
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================

    /// Add point to list.
    /// 
    /// Warning! List must not be full.
    pub fn add_point(&mut self, point: Point) {
        self.points[self.length] = point;
        self.length += 1;
    }
}
