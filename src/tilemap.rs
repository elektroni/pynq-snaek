//! Square 8x8 matrix of tiles.

use crate::xil;
use crate::print64;
use crate::println64;

use crate::tile::Tile as Tile;
use crate::tile::TileType as TileType;
use crate::pointlist::PointList as PointList;
use crate::point::Point as Point;

//==============================================================================
// GLOBAL CONSTANTS
//==============================================================================

pub const TILEMAP_WIDTH: usize = 8;
pub const TILEMAP_HEIGHT: usize = 8;

//==============================================================================
// TILEMAP DEFINITION
//==============================================================================

pub struct Tilemap {
    tiles: [[Tile; TILEMAP_WIDTH]; TILEMAP_HEIGHT],
}

impl Tilemap {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> Tilemap {
        Tilemap {
            tiles: [[Tile::new_empty(); TILEMAP_WIDTH]; TILEMAP_HEIGHT]
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn height(&self) -> usize {
        TILEMAP_HEIGHT
    }

    pub fn width(&self) -> usize {
        TILEMAP_WIDTH
    }

    /// Get a tile from given position.
    pub fn tile_at(&self, point: Point) -> Tile {
        self.tiles[point.y()][point.x()]
    }
    
    /// Check that given position is valid position.
    pub fn tile_exists(&self, point: Point) -> bool {
        let valid_x: bool = (0..self.width()).contains(&point.x());
        let valid_y: bool = (0..self.height()).contains(&point.y());
        valid_x && valid_y
    }
    
    /// Get list of empty tiles.
    pub fn free_points(&self) -> PointList {
        let mut points: PointList = PointList::new();        
        for y in 0..self.height() {
            for x in 0..self.width() {
                let position: Point = Point::new(x, y);
                match self.tile_at(position).ttype() {
                    TileType::Empty => {
                        if !points.is_full() {
                            points.add_point(position);
                        }
                        else {
                            println64!("free point list is full");
                        }
                    },
                    TileType::Snake => {},
                    TileType::Food => {},
                }
            }
        }
        points
    }
    
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================
    
    /// Convert tile at given position to given tile type.
    pub fn set_tile(&mut self, point: Point, tile: Tile) {
        let x: usize = point.x();
        let y: usize = point.y();
        self.tiles[y][x] = match tile.ttype() {
            TileType::Empty => Tile::new_empty(),
            TileType::Snake => Tile::new_snake(),
            TileType::Food => Tile::new_food(),
        }
    }
    
    /// Convert tile at given position to empty.
    pub fn clear_tile(&mut self, point: Point) {
        self.set_tile(point, Tile::new_empty());
    }
}
