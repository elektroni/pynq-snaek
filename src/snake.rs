//! Structure that contains and controls the state of a snake.

use crate::xil;
use crate::print64;
use crate::println64;

use crate::body::Body as Body;
use crate::part::Part as Part;
use crate::point::Point as Point;
use crate::part::Direction as Direction;

//==============================================================================
// SNAKE DEFINITION
//==============================================================================

pub struct Snake {
    body: Body,
}

impl Snake {

    // ===========
    // CONSTRUCTOR
    // ===========

    pub const fn new() -> Snake {
        Snake {
            body: Body::new(),
        }
    }

    // ================
    // PUBLIC INTERFACE
    // ================
    
    pub fn body(&self) -> &Body {
        &self.body
    }
    
    /// Shortcut reference to the snake head.
    pub fn head(&self) -> &Part {
        &self.body.head()
    }

    /// Check if snake part collides with other snake part.
    pub fn eats_itself(&self) -> bool {
        for index0 in 0..self.body.length() {
            if !self.body.part_exists(index0) {
                println64!("part {} does not exist", index0);
                return true;
            }

            let position0: Point = self.body.part_at(index0).position();
            for index1 in 0..self.body.length() {
                if !self.body.part_exists(index1) {
                    println64!("part {} does not exist", index1);
                    return true;
                }

                if index0 != index1 {
                    let position1: Point = self.body.part_at(index1).position();
                    if position0 == position1 {
                        println64!("snake parts {} and {} collide",
                            index0,
                            index1);
                        return true;
                    }
                }
            }
        }
        false
    }
        
    // ========================
    // PUBLIC MUTABLE INTERFACE
    // ========================

    /// Shortcut to set snake head direction.
    pub fn set_head_direction(&mut self, direction: Direction) {
        self.body.head_mut().set_direction(direction);
    }

    pub fn add_part(&mut self) -> bool {
        let tail: &Part = self.body.tail();
        let old_tail_position: Point = tail.position_old();
        let old_tail_direction: Direction = tail.direction_old();
        return self.body.add_part(old_tail_position, old_tail_direction);
    }

    /// Advance snake position.
    pub fn advance(&mut self) -> bool {        
        let mut ppd: Direction = self.body.head().direction();
        for index in 0..self.body.length() {
            if !self.body.part_exists(index) {
                println64!("could not move part {}, it does not exist", index);
                return false;
            }

            let part: &mut Part = self.body.part_at_mut(index);
            
            // save current state
            part.set_position_old(part.position());
            part.set_direction_old(part.direction());
            
            // update part position
            let move_is_ok: bool = match part.direction() {
                Direction::North => {
                    if usize::MIN < part.y() {
                        part.set_y(part.y() - 1);
                        true
                    }
                    else {
                        println64!("y too small");
                        false
                    }
                },
                Direction::South => {
                    if part.y() < usize::MAX {
                        part.set_y(part.y() + 1);
                        true
                    }
                    else {
                        println64!("y too big");
                        false
                    }
                },
                Direction::East => {
                    if part.x() < usize::MAX {
                        part.set_x(part.x() + 1);
                        true
                    }
                    else {
                        println64!("x too big");
                        false
                    }
                },
                Direction::West => {
                    if usize::MIN < part.x() {
                        part.set_x(part.x() - 1);
                        true
                    }
                    else {
                        println64!("x too small");
                        false
                    }
                },
            };

            if !move_is_ok {
                println64!("could not move from {} to {}",
                    part.position(),
                    part.direction());
                return false;
            }

            println64!("part {} moved from {} to {}",
                index,
                part.position_old(),
                part.position());

            // update part direction
            if index != 0 {
                // greater part direction
                part.set_direction(ppd);
                println64!("part {} direction changed from {} to {}",
                    index,
                    part.direction_old(),
                    part.direction());
            }

            ppd = part.direction_old();
        }
        true
    }
}
