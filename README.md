# PYNQ Snaek

## General Idea

A simple snake game programmed with Rust and cross-compiled to Pynq-Z1 during the course Bare Metal Rust.

## Participants

This project was built by a single developer with a code review and suggestions from the course arranger Hegza (Henri Lunnikivi) github.com/hegza/xil-custom-sys.

## Features per Credit (27 Hours of Work)

1. Credit: Basic snake movement, collision and food interaction logic was implemented, tested and debugged in Python, then converted into Rust.
2. Credit: Code was rewritten to conform Rust's borrow checking rules, basic idiomaticity rules and to interact with hardware.
3. Credit: Code was added more functionality such as pause/resume and reset routines, game status indication with leds, improved stability via error checking and tested and debugged.

## Difficulty Estimation

This project was fairly easy, but time consuming.
Game state logic itself was fairly simple to implement, but problem locating during debugging took time.
Hardware interaction itself was fairly complex to implement and to debug.

## Devices Used

This program uses the Pynq-Z1 found in the university classroom.
It has a Zynq-7000 SoC chip with ARM Cortex-A9 Processor in where this code is executed in and Artix-7 family FPGA.
It controls 8x8 RGB led matrix with 6-bit color correction values and 8-bit color values via M54564P/FP current source and DM163 current sink.

## Libraries Used

This program uses xil-sys by Hegza (Henri Lunnikivi) github.com/hegza/xil-custom-sys to cross-compile the Rust code.

## Setup

This program is targeted to ARMv7-A processor via command `rustup target add armv7a-none-eabi`.

## Usage instructions

Help the snake to eat so much food that it fills the screen.
Snake grows one new body part each time it eats a food with its head.
Snake dies if it hits the wall or eats a part of itself.
Snake has a green color and food has a red color.

Toggle switch 0 to resume or pause the game.
You can pause the game any time.
While paused, you can choose the direction of the snake head.
Toggle switch 1 to restart the game.
You can restart the game any time.
Press button 0 to change the snake direction to right.
Press button 1 to change the snake direction to up.
Press button 2 to change the snake direction to down.
Press button 3 to change the snake direction to left.

Single color leds indicate the time remaining for next snake movement.
When all leds are on, the movement happens at the next moment.
RGB leds indicate how close the snake is to the walls or the food.
If the color is red, snake is close to a wall.
Watch out!
If the color is green, snake is close to a food.
If the color is blue, snake is close to both walls and food.
